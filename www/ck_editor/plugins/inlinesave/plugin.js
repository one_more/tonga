CKEDITOR.plugins.add( 'inlinesave',
{
	init: function( editor )
	{
		editor.addCommand( 'inlinesave',
			{
				exec : function( editor )
				{

					addData();
					
					function addData() {
						var text = editor.getData();
						var container = $(editor.container.$);
                        var save_url = container.data('save-url');
						
						$.post(save_url, {text:text});
					} 

				}
			});
		editor.ui.addButton( 'Inlinesave',
		{
			label: 'Сохранить',
			command: 'inlinesave',
			icon: this.path + 'images/inlinesave.png'
		} );
	}
} );