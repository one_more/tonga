<?php
function  PHPMailerAutoload($file) {
    $file   = strtolower($file);
    $file   = 'class.'.$file.'.php';
    if(file_exists(ROOT_PATH.DS.'lib'.DS.'PHPMailer'.DS.$file)) {
        require_once($file);
    } else {
        return false;
    }
}

spl_autoload_register('PHPMailerAutoload');
