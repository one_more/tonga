<?php

class SiteRouter extends Router {
	use trait_controller, trait_starter_router;

	private $positions = [];

	public function __construct() {
		$this->routes = [
			'/' => [$this, 'index', 'no check'],
			'/login' => [$this, 'login'],
			'/logout' => [$this, 'logout'],
			'/edit_user' => ['UserController', 'edit_user'],
			'/add_user' => ['UserController', 'add_user'],
			'/save_text' => [$this, 'save_text'],
			'/mail' => [$this, 'mail'],
			'/upload_images' => [$this, 'upload_images', 'no_check'],
			'/language_model' => [$this, 'language_model', 'no check']
		];
	}

	public function index() {
		$this->show_result();
	}

	public function login() {
		$login = Request::get_var('login', 'string');
		$password = Request::get_var('password', 'string');
		$remember = Request::get_var('remember', 'string');
		$user_controller = Application::get_class('UserController');
		echo json_encode($user_controller->login($login, $password, (bool)$remember));
	}

	public function language_model() {
		$template = Application::get_class('Starter');
		echo file_get_contents($template->path.DS.'lang'.DS.CURRENT_LANG.DS.'client.json');
	}

	public function logout() {
		$user = Application::get_class('User');
		$user->log_out();
	}

	public function save_text() {
		$user_controller = Application::get_class('UserController');
		if($user_controller->is_admin()) {
			require_once(ROOT_PATH.DS.'lib'.DS.'HTMLPurifier'.DS.'HTMLPurifier.auto.php');
			$section = Request::get_var('section', 'string');
			$text = trim(Request::get_var('text'));
			$config = HTMLPurifier_Config::createDefault();
			$purifier = new HTMLPurifier($config);
			$text = $purifier->purify($text);
			$text_controller = Application::get_class('TextController');
			$text_controller->update_section($section, $text);
		}
	}

	public function mail() {
		$mailer = Application::get_class('Mailer');
		$name = Request::get_var('name', 'string');
		$email = Request::get_var('email', 'email');
		$message = Request::get_var('message', 'string');
		$message = "from: {$name}\nemail: {$email}\n{$message}";
		try {
			$mailer->send($message);
		} catch(Exception $e) {
			Error::log($e->getMessage());
		}
	}

	public function upload_images() {
		$user_controller = Application::get_class('UserController');
		if($user_controller->is_admin()) {
			$images_controller = Application::get_class('ImagesController');
			if(isset($_FILES['upload'])) { //upload from ck_editor
				try {
					$url = $images_controller->upload_image($_FILES['upload']);
					$CKEditorFuncNum = Request::get_var('CKEditorFuncNum', 'int');
					echo "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url')</script>";
				} catch(Exception $e) {
					echo "<script>{$e->getMessage()}</script>";
				}
			}
		}
	}

	private function show_result() {
		if(!Request::is_ajax()) {
			$template   = Application::get_class('Starter');
			$templator = new Smarty();
			$static_path = DS.'starter';
			$static_paths = [
				'css_path' => $static_path.DS.'css',
				'images_path' => $static_path.DS.'images',
				'js_path' => $static_path.DS.'js'
			];
			$text_controller = Application::get_class('TextController');
			$templator->assign($text_controller->get_sections());
			$works_controller = Application::get_class('WorksController');
			$templator->assign('works', $works_controller->get_works());
			$templator->assign('current_year', date('Y'));
			$user_controller = Application::get_class('UserController');
			$templator->assign('is_admin', $user_controller->is_admin());
			$templator->assign($static_paths);
			$templator->setTemplateDir($template->path.DS.'templates'.DS.'index');
			$templator->setCompileDir($template->path.DS.'templates_c');
			$templator->assign($this->positions);
			echo $templator->getTemplate('index.tpl.html');
		} else {
			$this->positions = array_filter($this->positions, function($el) {
				return $el !== null;
			});
			echo json_encode($this->positions);
		}
	}
}