<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-05-18 17:33:42
         compiled from "/var/www/tonga.my/templates/starter/templates/index/index.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:417258837555613696501b4-31766358%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7365034175c1952d367759dba669e9f4d9368ecb' => 
    array (
      0 => '/var/www/tonga.my/templates/starter/templates/index/index.tpl.html',
      1 => 1431954740,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '417258837555613696501b4-31766358',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_555613696c5629_14940052',
  'variables' => 
  array (
    'images_path' => 0,
    'css_path' => 0,
    'js_path' => 0,
    'is_admin' => 0,
    'about_text' => 0,
    'works' => 0,
    'work' => 0,
    'works_text' => 0,
    'contacts_text' => 0,
    'current_year' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_555613696c5629_14940052')) {function content_555613696c5629_14940052($_smarty_tpl) {?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <link href='<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/favicon.ico' rel='shortcut icon'>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['css_path']->value;?>
/site.css">
    <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
/html5.js"><?php echo '</script'; ?>
>
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['css_path']->value;?>
/ie.css" type="text/css" media="all">
    <![endif]-->
    <!--[if lt IE 8]>
        <div style=' clear: both; text-align:center; position: relative;'>
            <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
        </div>
    <![endif]-->
    <?php echo '<script'; ?>
 async="async" src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
/site.js" ><?php echo '</script'; ?>
>
</head>
<body>
<div class="glob">
<div class="page_spinner"><span></span></div>
<div class="main">
<div class="center">
<!--header -->
<header>
    <div class="logoHolder">
        <a href="#!/pageHome" id="logo">
            <p>tonga lafatra</p>
            <p>web development team</p>
        </a>
    </div>
</header>
<!--header end-->
<!--content -->
<article id="content">
    <ul>
        <li id="pageHome">
            <div class="menuHolder">
                <nav class="menu">
                    <ul id="menu">
                        <li>
                            <a href="#!/pageHome">
                                <div class="hintHolder">
                                    <div class="mText">home</div>
                                </div>
                                <div class="mSymbl">H</div>
                                <div class="extra2"></div>
                            </a>                                    </li>
                        <li class="with_ul">
                            <a href="#!/pageAbout">
                                <div class="hintHolder">
                                    <div class="mText">о нас</div>
                                </div>
                                <div class="mSymbl">O</div>
                                <div class="extra2"></div>
                            </a>
                            <!--
                            <ul class="submenu_1">
                                <li class="padTop">
                                    <a href="#!/pageMore">welcome</a>
                                    <span class="extra1"></span>
                                </li>
                                <li><a href="#!/pageMore">services</a></li>
                                <li class="padBot"><a href="#!/pageMore">team</a></li>
                            </ul>
                            -->
                            <div class="sprite1"></div>
                        </li>
                        <li>
                            <a href="#!/pageWorks">
                                <div class="hintHolder">
                                    <div class="mText">наши работы</div>
                                </div>
                                <div class="mSymbl">H</div>
                                <div class="extra2"></div>
                            </a>
                            <div class="sprite1"></div>
                        </li>
                        <li>
                            <a href="#!/pageContact">
                                <div class="hintHolder">
                                    <div class="mText">контакты</div>
                                </div>
                                <div class="mSymbl">K</div>
                                <div class="extra2"></div>
                            </a>
                            <div class="sprite1"></div>
                        </li>
                    </ul>
                </nav>
            </div>
        </li>
        <li id="pageAbout">
            <div class="box">
                <div class="closeHolder">
                    <a class="closeBtn" href="#!/pageHome">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/closeBtn.png" alt="">
                    </a>
                </div>
                <div class="containerContent">
                    <div class="col1">
                        <?php if ($_smarty_tpl->tpl_vars['is_admin']->value) {?>
                        <div contenteditable="true" data-save-url="/save_text?section=about_text">
                        <?php } else { ?>
                        <div>
                        <?php }?>
                            <?php echo $_smarty_tpl->tpl_vars['about_text']->value;?>

                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li id="pageWorks">
            <div class="box">
                <div class="closeHolder">
                    <a class="closeBtn" href="#!/pageHome">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/closeBtn.png" alt="">
                    </a>
                </div>
                <div class="containerContent">
                    <div class="col1">
                        <h2>Наши работы</h2>
                        <?php if (count($_smarty_tpl->tpl_vars['works']->value)) {?>
                        <div id="jcarousel_1">
                            <ul>
                                <?php  $_smarty_tpl->tpl_vars['work'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['work']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['works']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['work']->key => $_smarty_tpl->tpl_vars['work']->value) {
$_smarty_tpl->tpl_vars['work']->_loop = true;
?>
                                    <li>
                                        <figure class="_fig1">
                                            <a class="fancyPic" rel="Appendix" href="<?php echo $_smarty_tpl->tpl_vars['work']->value['big_picture'];?>
">
                                                <span class="zoomSp"></span>
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['work']->value['small_picture'];?>
" alt="">                                                    </a>
                                            <figcaption>
                                                <p>
                                                    <a class="_link3" href="#"><?php echo $_smarty_tpl->tpl_vars['work']->value['link'];?>
</a>
                                                    <br>
                                                    <?php echo $_smarty_tpl->tpl_vars['work']->value['description'];?>

                                                </p>
                                            </figcaption>
                                        </figure>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="btnHolder">
                            <div class="scrButn left">
                                <a href="#" class="prevBtn">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/prevBtn.png" alt="">
                                </a>
                            </div>
                            <div class="scrButn right">
                                <a href="#" class="nextBtn">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/nextBtn.png" alt="">
                                </a>
                            </div>
                        </div>
                        <?php } else { ?>
                            <?php if ($_smarty_tpl->tpl_vars['is_admin']->value) {?>
                            <div contenteditable="true" data-save-url="/save_text?section=works_text">
                            <?php } else { ?>
                            <div>
                            <?php }?>
                                <?php echo $_smarty_tpl->tpl_vars['works_text']->value;?>

                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </li>
        <li id="pageContact">
            <div class="box">
                <div class="closeHolder">
                    <a class="closeBtn" href="#!/pageHome">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/closeBtn.png" alt="">
                    </a>
                </div>
                <div class="containerContent">
                    <div class="col1">
                        <div class="col4 padRight2">
                            <h2>Контакты</h2>
                            <div class="col6 padRight3">
                                <img width="300" height="268" src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/contacts.jpg" alt=""/>
                            </div>
                            <div class="col7">
                                <p <?php if ($_smarty_tpl->tpl_vars['is_admin']->value) {?>contenteditable="true" data-save-url="/save_text?section=contacts_text"<?php }?>>
                                   <?php echo $_smarty_tpl->tpl_vars['contacts_text']->value;?>

                                </p>
                            </div>
                        </div>
                        <div class="col5">
                            <h2>Напишите нам</h2>
                            <div class="wrapper">
                                <form action="#" id="ContactForm">
                                    <div class="success"> Спасибо!&nbsp;Ваше письмо отправлено.</div>
                                    <fieldset class="left">
                                        <div class="block">
                                            <label class="name">
                                                <span class="bg">
                                                    <input type="text" name="name" value="Имя:" class="input marNone">
                                                </span>
                                                <span class="error">*Некорректное имя.</span>
                                                <span class="empty">*Заполните поле.</span>
                                            </label>
                                            <label class="email">
                                                <span class="bg">
                                                    <input type="email" name="email" value="E-mail:" class="input marNone">
                                                </span>
                                                <span class="error">*Некорректный email.</span>
                                                <span class="empty">*Заполните поле.</span></label>
                                        </div>
                                        <div class="block">
                                            <label class="message">
                                                <span class="bg">
                                                    <textarea name="message" rows="1" cols="2">Сообщение:</textarea>
                                                </span>
                                                <span class="error">*Сообщение слишком короткое.</span>
                                                <span class="empty">*Заполните поле.</span> </label>
                                        </div>
                                        <div class="formButtons">
                                            <div class="formBtn">
                                                <a href="#" data-type="reset" class="more">Очистить</a>                                                          </div>
                                            <div class="formBtn">
                                                <a href="#" data-type="submit" class="more">Отправить</a>                                                          </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</article>
<!--content end-->
<!--footer -->
<footer>
    <!-- FOOTER_LINK -->
    <div class="mainFooter">
        <div class="privHold">
            <pre class="textPrivacy"><strong>tonga &copy; <?php echo $_smarty_tpl->tpl_vars['current_year']->value;?>
</strong></pre>
            <!--
            <div class="followHolder">
                <ul>
                    <li><a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/followIcon1.png" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/followIcon2.png" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/followIcon3.png" alt=""></a></li>
                </ul>
            </div>
            -->
        </div>
    </div>
</footer>
<!--footer end-->
</div>
</div>
</div>
<?php echo '<script'; ?>
 src="/ck_editor/ckeditor.js"><?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
