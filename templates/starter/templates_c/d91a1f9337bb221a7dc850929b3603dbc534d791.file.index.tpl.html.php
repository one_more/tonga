<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-05-15 22:08:31
         compiled from "/var/www/tonga.my/templates/starter/templates/admin_panel/index.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:9306532865556442f3f8566-48648732%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd91a1f9337bb221a7dc850929b3603dbc534d791' => 
    array (
      0 => '/var/www/tonga.my/templates/starter/templates/admin_panel/index.tpl.html',
      1 => 1431632590,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9306532865556442f3f8566-48648732',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'css_path' => 0,
    'images_path' => 0,
    'navbar' => 0,
    'left_menu' => 0,
    'main_content' => 0,
    'js_path' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5556442f407b29_33490844',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5556442f407b29_33490844')) {function content_5556442f407b29_33490844($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
    <title>Admin panel</title>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['css_path']->value;?>
/main.css"/>
    <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/favicon.ico"/>
</head>
<body>
    <?php echo $_smarty_tpl->tpl_vars['navbar']->value;?>

    <div class="container-fluid">
        <div class="row">
            <div id="left_menu" class="col-lg-2 col-lg-offset-1">
                <?php echo $_smarty_tpl->tpl_vars['left_menu']->value;?>

            </div>
            <div id="main_content" class="col-lg-7">
                <?php echo $_smarty_tpl->tpl_vars['main_content']->value;?>

            </div>
        </div>
    </div>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
/main.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
/admin_panel_views.js"><?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
