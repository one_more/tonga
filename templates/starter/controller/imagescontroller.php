<?php

class ImagesController {
	use trait_controller;

	private $upload_dir;
	private $max_size;
	private $allowed_types = ['bmp', 'jpg', 'jpeg', 'png'];

	public function __construct() {
		$this->upload_dir = ROOT_PATH.DS.'www'.DS.'starter'.DS.'images';
		$this->max_size = (1024*1024)*2;
	}

	public function upload_image($img) {
		$errors = $this->get_lang_vars();
		list($type,$img_type) = explode('/', $img['type']);
		if($type != 'image') {
			throw new Exception($errors['not_image']);
		}
		if(file_exists($img['tmp_name'])) {
			$extension = end(explode('.', $img['name']));
			if(!in_array($img_type, $this->allowed_types)) {
				throw new Exception($errors['forbidden_type']);
			}
			if($img['size'] > $this->max_size) {
				throw new Exception($errors['big_size']);
			}
			$tmp_file_hash = md5(file_get_contents($img['tmp_name']));
			$new_file = $this->upload_dir.DS."{$tmp_file_hash}.{$extension}";
			if(!move_uploaded_file($img['tmp_name'], $new_file)) {
				throw new Exception($errors['cant_load_file']);
			}
			@chmod($new_file, 0777);
		} else {
			throw new Exception($errors['cant_load_file']);
		}
		return str_replace(ROOT_PATH.DS.'www', '', $new_file);
	}
}