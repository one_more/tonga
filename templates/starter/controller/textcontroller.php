<?php

class TextController {
	use trait_controller, trait_json;

	public function get_sections() {
		return $this->get_lang_vars();
	}

	public function update_section($section, $text) {
		$sections = $this->get_sections();
		if(!empty($sections[$section]) && trim($text)) {
			$sections[$section] = $text;
			$this->save_sections($sections);
		}
	}

	private function save_sections($sections) {
		foreach($sections as &$section) {
			$section = str_replace(['"', "\n"], ['\"', ''], $section);
		}
		$file = $this->get_lang_vars_file();
		if(file_exists($file)) {
			file_put_contents($file, $this->array_to_json_string($sections));
		}
	}
}