//--------global-------------
var isSplash = true;
var isAnim = true;

var spinner;
var mapSpinner;
var MSIE = ($.browser.msie) && ($.browser.version <= 8)


//------DocReady-------------
$(document).ready(function(){ 
    if(location.hash.length == 0){
        location.hash="!/"+$('#content > ul > li:first-child').attr('id');
    }
    ///////////////////////////////////////////////////////////////////
        loaderInit();
function loaderInit(){
        var opts = {
              lines: 13,
              length: 13,
              width: 6,
              radius: 14,
              rotate: 0,
              color: '#434343',
              speed: 1.3,
              trail: 60,
              shadow: false,
              hwaccel: false,
              className: 'spinner',
              zIndex: 2e9,
              top: 'auto',
              left: 'auto'
        };
        var target = $(".page_spinner > span");
        spinner = new Spinner(opts).spin();
        target.append(spinner.el)
        ///////////////////////////////////////
                var opts2 = {
              lines: 12,
              length: 6,
              width: 3,
              radius: 8,
              rotate: 0,
              color: '#000',
              speed: 1.3,
              trail: 60,
              shadow: false,
              hwaccel: false,
              className: 'spinner',
              zIndex: 2e9,
              top: 'auto',
              left: 'auto'
        };
        var target2 = $(".google_map > span");
        mapSpinner = new Spinner(opts2).spin();
        target2.append(mapSpinner.el)

}

///////////////////////////////////////////////////////////////////

     $('ul#menu').superfish({
          delay:       800,
          animation:   {opacity:'show'},
          speed:       600,
          autoArrows:  false,
         dropShadows: false,
         	onInit: function(){
  				$("#menu > li > a").each(function(index){
  					var conText = $(this).find('.mText').text();
                      // $(this).append("<div class='_area'></div><div class='mTextOver'>"+conText+"</div>"); 
                       $(this).append("<div class='_area'></div>"); 
  				})
  	 		}
        });
});
  
 //------WinLoad-------------  
$(window).load(function(){  

$(".followHolder > ul > li > a").hoverSprite({onLoadWebSite: true});
$(".closeBtn").hoverSprite({onLoadWebSite: true});
$(".prevBtn").hoverSprite({onLoadWebSite: true});
$(".nextBtn").hoverSprite({onLoadWebSite: true});

//$('.more').sprites({method:'gStretch',hover:true});
   
   	
    $(".extra2").slideUp(1)
    $(".mSymbl").slideUp(1)
    setTimeout(spriteAnim1, 800);

		function spriteAnim1(){
			$('.sprite1').sprite({fps: 18, no_of_frames: 36, play_frames: 36, start_at_frame:0});
            $(".extra2").slideDown(300);
            
            setTimeout(function(){$(".mSymbl").slideDown(400); isAnim=false;}, 1400)
            
		}
        
 
   
   
   $("#jcarousel_1").jCarouselLite({
        btnNext: ".nextBtn",
        btnPrev: ".prevBtn",
        speed: 800,
        visible: 3
    });
       
       $('.fancyPic').fancybox({'titlePosition': 'inside', 'overlayColor':'#000'});    
   
     if(!MSIE){ $('.fancyPic').find(".zoomSp").fadeTo(500, 0)}else{ $('.fancyPic').find(".zoomSp").css({"display":"none"})  }
    $('.fancyPic').hover(function(){
    if(!MSIE){ 
        $(this).find(".zoomSp").stop().fadeTo(500, 1)
    }else{
        $(this).find(".zoomSp").css({"display":"block"})   
    }
    },
     function(){
            if(!MSIE){ 
                $(this).find(".zoomSp").stop().fadeTo(500, 0)
            }else{
                     $(this).find(".zoomSp").css({"display":"none"})    
            }   
        }
 )     
       
 $('.more').hover(
	function(){
 		$(this).animate({color:"#ebd370"},300)
   },
   function(){
   		$(this).animate({color:"#fff"},300)
        }
    )
       
var menuItems = $('#menu >li'); 
var currentIm = 0;
var lastIm = 0;

navInit();
function navInit(){
    $(".hintHolder").stop(true).animate({top:"-100px", opacity:0}, 1);
}

///////////////////////////////////////////////
    var navItems = $('.menu > ul >li');

    $('.menu > ul >li').eq(0).css({'display':'none'});
	var content=$('#content'),
		nav=$('.menu');

    	$('#content').tabs({
		preFu:function(_){
			_.li.css({left:"-1700px",'display':'none'});
		}
		,actFu:function(_){			
			if(_.curr){
				_.curr.css({'display':'block', left:'1700px'}).stop().delay(200).animate({left:"0px"},700,'easeOutCubic');
                
                cont_resize(_.n);
                if ((_.n == 0) && ((_.pren>0) || (_.pren==undefined))){splashMode();}
                if (((_.pren == 0) || (_.pren == undefined)) && (_.n>0) ){contentMode(); }
            }
			if(_.prev){
			     _.prev.stop().animate({left:'-1700px'},700,'easeInOutCubic',function(){_.prev.css({'display':'none'});} );
             }
		}
	})
    

    function splashMode(){
        isSplash = true;
       
    }
    
    function contentMode(){  
        isSplash = false;

    }
    
    function cont_resize(_page){
        
        var li_W = $('#content > ul > li').eq(_page).height();

        $('#content').stop().animate({height:li_W+"px"}, 600, 'easeInOutCubic', function(){centrRepos();} ).css({'overflow':'visible'})
        
    }		
    
	nav.navs({
			useHash:true,
             hoverIn:function(li){
                if(isAnim==false){
                    
                    $(".hintHolder", li).stop(true).animate({top:"-56px", opacity:1}, 600, 'easeOutCubic');
                    $(".mText", li).stop(true).animate({opacity:1}, 600, 'easeOutCubic');
                }
                   // if(($.browser.msie) && ($.browser.version <= 8)){}else{}
             },
                hoverOut:function(li){
                    if ((!li.hasClass('with_ul')) || (!li.hasClass('sfHover'))) {
                        $(".hintHolder", li).stop(true).animate({top:"-100px", opacity:0}, 300, 'easeInCubic');
                        $(".mText", li).stop(true).animate({opacity:0}, 300, 'easeOutCubic');
                    } 
                } 
		})

		.navs(function(n){			
			$('#content').tabs(n);
		});

//////////////////////////////////////////
    
   	var h_cont;
  
	function centrRepos() {
         h_cont = $('.center').height();
         $('body').css({'min-height':h_cont+30+'px'});
		var h=$(window).height();
		if (h>(h_cont+40)) {
			m_top=~~(h-h_cont)/2;
			h_new=h;
		} else {
			m_top=20;
			h_new=h_cont+40;
		}
		$('.center').stop().animate({paddingTop:m_top},600,'easeOutCubic');
        
        

	}
	centrRepos();
    ///////////Window resize///////
    
    function windowW() {
        return (($(window).width()>=parseInt($('body').css('minWidth')))?$(window).width():parseInt($('body').css('minWidth')));
    }
    
    
	$(window).resize(function(){
        centrRepos();
         
        }
    );

    } //window function
); //window load

$(window).load(function() {
    'use strict';

    $('.page_spinner').fadeOut();
    $('body').css({overflow:'auto', 'min-height':'820px'});

    CKEDITOR.on('instanceReady', function(ev) {
        var editor = ev.editor;
        editor.setReadOnly(false);
    })
});